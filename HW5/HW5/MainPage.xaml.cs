﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using HW5;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using System.Collections.ObjectModel;


namespace HW5
{
    public partial class MainPage : ContentPage
    {
        //ProductInfo productInfoJson = new ProductInfo();
        //ProductInfo productInfoJson = new List<ProductInfo>();
        public List<ProductInfo> productList;

        public MainPage()
        {
            InitializeComponent();
            ReadJson();
        }

        async void MenuItem_Clicked(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            var itemSelected = item.CommandParameter as ProductInfo;
            await Navigation.PushAsync(new MoreInfo(itemSelected));

        }
        private void ReadJson()
        {
            var FileName = "HW5.products.json";

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(FileName);

            ProductInfo data;
            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                //IEnumerable<ProductInfo> productInfoJson = JsonConvert.DeserializeObject<IEnumerable<ProductInfo>>(jsonAsString);
                productList = JsonConvert.DeserializeObject<List<ProductInfo>>(jsonAsString);

            }
            productListView.ItemsSource = productList;

           
        }
    }
    
}
